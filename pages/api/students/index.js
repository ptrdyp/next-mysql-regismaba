import { prisma } from "@/config/db";

export default async function handler(req, res) {
    switch(req.method) {
        case "GET" :
            return await getStudent(req, res);

        case "POST" :
            return await addStudent(req, res);
    }
}

const getStudent = async (req, res) => {

    try{
        const result = await prisma.Student.findMany();
        return res.status(200).json(result);
    }catch(error){
        return res.status(500).json(error);
    }
    
}

const addStudent = async (req, res) => {

    try{

        const {
            nama, 
            nik, 
            tempat_lahir, 
            tanggal_lahir,
            jenis_kelamin,
            kewarganegaraan,
            agama,
            nama_ibu,
            email,
            no_telp,
            alamat,
            kode_pos,
            provinsi,
            kabupaten,
            kecamatan,
            pendidikan,
            sekolah,
            nilai,
            program_studi_1,
            program_studi_2,
            image
        } = req.body;

        const data = {
            nama,
            nik,
            tempat_lahir,
            tanggal_lahir: `${tanggal_lahir}T00:00:00.000Z`,
            jenis_kelamin,
            kewarganegaraan,
            agama,
            nama_ibu,
            email,
            no_telp,  
            alamat,
            kode_pos, 
            provinsi,  
            kabupaten,   
            kecamatan,
            pendidikan,
            sekolah,
            nilai,
            program_studi_1,
            program_studi_2,
            image
        }

        const result = await prisma.Student.create({
            data,
            select : {
                id : true
            }
        })

        return res.status(200).json(result);
    }catch(error){
        return res.status(500).json(error);
    }
    
}