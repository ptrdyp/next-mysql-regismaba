import { prisma } from "@/config/db";

export default async function handler(req, res) {
    switch(req.method) {
        case "GET" :
            return await getStudentById(req, res);

        case "PUT" :
            return await updateStudent(req, res);

        case "DELETE" :
            return await deleteStudent(req, res);
    }
}

const getStudentById = async (req, res) => {

    try{

        const {studentId} = req.query;

        const result = await prisma.Student.findFirst({
            where : {
                id : {
                    equals : parseInt(studentId)
                }
            }, 
        });

        return res.status(200).json(result);
        
    }catch(error){
        return res.status(500).json(error.message);
    }
    
}

const updateStudent = async (req, res) => {

    try{

        const {
            nama, 
            nik, 
            tempat_lahir, 
            tanggal_lahir,
            jenis_kelamin,
            kewarganegaraan,
            agama,
            nama_ibu,
            email,
            no_telp,
            alamat,
            kode_pos,
            provinsi,
            kabupaten,
            kecamatan,
            pendidikan,
            sekolah,
            nilai,
            program_studi_1,
            program_studi_2,
            image
        } = req.body;

        const {studentId} = req.query;
        const result = await prisma.Student.update({
            where : {
                id : parseInt(studentId)
            }, 
            data : {
                nama,
                nik,
                tempat_lahir,
                tanggal_lahir: `${tanggal_lahir}T00:00:00.000Z`,
                jenis_kelamin,
                kewarganegaraan,
                agama,
                nama_ibu,
                email,
                no_telp,  
                alamat,
                kode_pos, 
                provinsi,  
                kabupaten,   
                kecamatan,
                pendidikan,
                sekolah,
                nilai,
                program_studi_1,
                program_studi_2,
                image
            }
        });

        return res.status(200).json(result);
        
    }catch(error){
        return res.status(500).json(error.message);
    }
    
}

const deleteStudent = async (req, res) => {

    try{
        const {studentId} = req.query;
        const result = await prisma.Student.delete({
            where : {
                id : parseInt(studentId)
            }
        });

        return res.status(200).json({success : true});
        
    }catch(error){
        return res.status(500).json(error);
    }
    
}