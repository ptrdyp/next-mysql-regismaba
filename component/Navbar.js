import AppContext from "@/context/appContext";
import CheckedContext from "@/context/checkedContext";
import { useContext } from "react";

function Navbar({searchQuery, setSearchQuery, setAlert, setCurrentPage}) {

    const value = useContext(AppContext);
    const CheckedContextData = useContext(CheckedContext);
    const checkedIds = CheckedContextData.checkedStudent;
    
    async function handleMultiDelete(e) {
        const reqOptions = {
            headers : {"Content-Type" : "application/json"},
            method : "DELETE",
            body : JSON.stringify({ids : checkedIds })
        }

        const response = await fetch(`${process.env.NEXT_PUBLIC_HOST}/api/students/deleteMulti`, reqOptions);
        const result = await response.json();

        if("ids" in result) {
            setAlert("Data mahasiswa yang dipilih berhasil dihapus");

            const newStudents = value.students.filter(student => {
                return result.ids.indexOf(student.id) === -1;
            })

            value.setTheStudents(newStudents);
        }
    }

    const handleSearch = (e) => {
        setSearchQuery(e.target.value);
        setCurrentPage(1); 
    }

    return (
        <div className="table-title">
			<div className="row">
				<div className="col-sm-6">
					<h2>Daftar Mahasiswa Baru</h2>
				</div>
				<div className="col-sm-6">
                    <input 
                        value={searchQuery}
                        onChange={handleSearch}
                        type = "text" 
                        className = "form-control" 
                        style = {{width : "200px", float : "right", height : "34px"}} 
                        name = "search_student" 
                        placeholder = "Cari Nama Mahasiswa" />						
				</div>
			</div>
            <div className="row mt-2">
                <div className="col-sm-12">
                    <a href="#addStudentModal" className="btn btn-success" data-toggle="modal"><i className="material-icons">&#xE147;</i> <span>Tambah Mahasiswa Baru</span></a>
					<a href="#" className="delete_all_data btn btn-danger" onClick={async (e) => await handleMultiDelete(e)}><i className="material-icons">&#xE15C;</i> <span>Hapus</span></a>
                </div>
            </div>
		</div>
    )
}   

export default Navbar;