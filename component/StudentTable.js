import { useContext, useState } from "react";
import Student from "./Student";
import CheckedContext from "@/context/checkedContext";

function StudentTable({students, handleDelete, setEditStudent}) {

    const [checkedAll, setCheckedAll] = useState(false);
    const value = useContext(CheckedContext);

    const handleSelectAllChange = (e) => {
        const {checked} = e.target;

        let checkedAllStudent;

        let checkboxes = document.querySelectorAll("table tbody input[type='checkbox']");
        
        if(checked) {
            setCheckedAll(true);

            checkboxes.forEach(checkbox => {
                checkbox.checked = true;
            })

            checkedAllStudent = [];

            students.map(student => {
                checkedAllStudent.push(student.id);
            })

        } else {
            setCheckedAll(false);
            checkboxes.forEach(checkbox => {
                checkbox.checked = false;
            })
            checkedAllStudent = [];
        }

        value.setCheckedStudent(checkedAllStudent);

    }

    const studentGenerator = () => {
        return (
            <>
                {
                    students.map(student => {
                        return (
                            <Student checkedAll = {checkedAll} setCheckedAll = {setCheckedAll} setEditStudent = {setEditStudent} key={student.id} student = {student} handleDelete = {handleDelete} />
                        )
                    })
                }
            </>
        )
    }
    return (
        <table className="table table-striped table-hover">
            <thead>
                <tr>
                    <th>
                        <span className="custom-checkbox">
                            <input type="checkbox" id="selectAll" onChange={(e) => handleSelectAllChange(e)} value={checkedAll} />
                            <label htmlFor="selectAll"></label>
                        </span>
                    </th>
                    <th>Image</th>
                    <th>Name</th>
                    <th>No Telp</th>
                    <th style={{paddingLeft: '30px', paddingRight: '30px'}}>Actions</th>
                </tr>
            </thead>
            <tbody>
                {studentGenerator()}
            </tbody>
        </table>
    )
}

export default StudentTable;