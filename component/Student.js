import CheckedContext from "@/context/checkedContext";
import { useContext } from "react";

function Student({student, handleDelete, setEditStudent, checkedAll, setCheckedAll}) {

    const value = useContext(CheckedContext);

    const fetchStudent = async (studentId) => {
        const response = await fetch(`${process.env.NEXT_PUBLIC_HOST}/api/students/` + studentId);
        const result = await response.json();

        result.tanggal_lahir = result.tanggal_lahir.split('T')[0];

        setEditStudent(result);
    }

    const handleChangeChecked = ({target}, studentId) => {
        const {checked} = target;

        if(checkedAll && !checked) {
            setCheckedAll(false);
        }

        if(checked) {
            value.setCheckedStudent([...value.checkedStudent, studentId]);
        } else {
            const newCheckedStudent = value.checkedStudent.filter(student => {
                return student.id != studentId;
            })

            value.setCheckedStudent(newCheckedStudent);
        }
    }

    return (
        <tr>
            <td>
                <span className="custom-checkbox">
                    <input type="checkbox" id="data_checkbox" onChange={(e) => handleChangeChecked(e, student.id)} className="data_checkbox" name="data_checkbox" value="" />
                    <label htmlFor="data_checkbox"></label>
                </span>
            </td>
            <td>
                <img src={student.image} alt="Student Image" width="50" height="50" />
            </td>
            <td>{student.nama}</td>
            <td>{student.no_telp}</td>
            <td>
                <a href="#editStudentModal" onClick={() => fetchStudent(student.id)} className="edit" data-toggle="modal"><i className="material-icons" data-toggle="tooltip" title="Edit">&#xE254;</i></a>
                <a href="#deleteStudentModal" onClick={() => handleDelete(student.id)} className="delete" data-toggle="modal"><i className="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i></a>
            </td>
        </tr>
    )
}

export default Student;