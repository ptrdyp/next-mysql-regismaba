import Alert from "./Alert";
import StudentTable from "./StudentTable";
import Pagination from "./Pagination";
import Navbar from "./Navbar";
import AppContext from "@/context/appContext";
import React, { useContext, useState } from "react";
import { Paginate } from "@/helpers/paginate";
import { CldUploadButton, CldUploadWidgetResults } from 'next-cloudinary';
import Image from 'next/image';
import { Search } from "@/helpers/search";
import CheckedContext from "@/context/checkedContext";

function Layout() {

  const value = useContext(AppContext);
  
  const [checkedStudent, setCheckedStudent] = useState("")
  console.log(checkedStudent);
  const [alert, setAlert] = useState("");

  const [saveStudent, setSaveStudent] = useState({
    nama : "", 
    nik : "", 
    tempat_lahir : "", 
    tanggal_lahir : "",
    jenis_kelamin : "",
    kewarganegaraan : "",
    agama : "",
    nama_ibu : "",
    email : "",
    no_telp : "",
    alamat : "",
    kode_pos : "",
    provinsi : "",
    kabupaten : "",
    kecamatan : "",
    pendidikan : "",
    sekolah : "",
    nilai : "",
    program_studi_1 : "",
    program_studi_2 : "",
    image : ""
  })

  const [editStudent, setEditStudent] = useState({
    id : "",
    nama : "", 
    nik : "", 
    tempat_lahir : "", 
    tanggal_lahir : "",
    jenis_kelamin : "",
    kewarganegaraan : "",
    agama : "",
    nama_ibu : "",
    email : "",
    no_telp : "",
    alamat : "",
    kode_pos : "",
    provinsi : "",
    kabupaten : "",
    kecamatan : "",
    pendidikan : "",
    sekolah : "",
    nilai : "",
    program_studi_1 : "",
    program_studi_2 : "",
    image : ""
  })

  const [searchQuery, setSearchQuery] = useState("");
  const [currentPage, setCurrentPage] = useState(1);
  const pageSize = 4;

  const onPageChange = (page) => {
    setCurrentPage(page);
  }

  let searchedResult;
  let paginatedStudent;
  if(searchQuery.length > 0) {
    searchedResult = Search(value.students, searchQuery);
    paginatedStudent = Paginate(searchedResult, currentPage, pageSize);
  } else {
    paginatedStudent = Paginate(value.students, currentPage, pageSize);
  }

  const handleSaveChange = ({target : { name, value }}) => {
    setSaveStudent({
      ...saveStudent, 
      [name] : value,
    });
  }

  /**
   * @param {CldUploadWidgetResults} result
   */
  const handleImageUpload = (result) => {
    console.log('result', result);
  
    if (result && result.info && result.info.url) {
      const url = result.info.url.toString()
      setSaveStudent(prevState => ({
        ...prevState,
        image: url
      }));
      console.log('Image URL:', url);
    }
  }

  const handleAddSubmit = async (e) => {
    e.preventDefault();

    const reqOption = {
      method : "POST",
      headers : {"Content-Type" : "application/json"},
      body : JSON.stringify(saveStudent)
    }

    console.log(saveStudent);

    const response = await fetch(`${process.env.NEXT_PUBLIC_HOST}/api/students`, reqOption);
    const result = await response.json();

    console.log(response);

    if (result) {
      setAlert("Mahasiswa baru berhasil ditambahkan");
      document.getElementsByClassName("addCancel")[0].click();
      const newStudents = [...value.students, result];

      value.setTheStudents(newStudents);
    }

    setSaveStudent({
      nama : "", 
      nik : "", 
      tempat_lahir : "", 
      tanggal_lahir : "",
      jenis_kelamin : "",
      kewarganegaraan : "",
      agama : "",
      nama_ibu : "",
      email : "",
      no_telp : "",
      alamat : "",
      kode_pos : "",
      provinsi : "",
      kabupaten : "",
      kecamatan : "",
      pendidikan : "",
      sekolah : "",
      nilai : "",
      program_studi_1 : "",
      program_studi_2 : "",
      image : ""
    })
  }

  const handleDelete = async (studentId) => {
    var reqOption = {
      method : "DELETE"
    }

    var response = await fetch(`${process.env.NEXT_PUBLIC_HOST}/api/students/` + studentId, reqOption);
    var result = await response.json();

    if (result) {
      setAlert("Data Mahasiswa baru berhasil dihapus");
      var prevStudents = value.students;
      var newStudents = prevStudents.filter(student => {
        return student.id != studentId;
      })
      value.setTheStudents(newStudents);
    }
  }

  const handleEditChange = ({target : {name, value}}) => {
    setEditStudent({...editStudent, [name] : value});
  }

  const handleEditSubmit = async (e) => {
    e.preventDefault();

    const reqOption = {
      method : "PUT",
      headers : {"Content-Type" : "application/json"},
      body : JSON.stringify(editStudent)
    }

    console.log(editStudent);

    const response = await fetch (`${process.env.NEXT_PUBLIC_HOST}/api/students/` + editStudent.id, reqOption);
    const result = await response.json()
    console.log(response);

    if (result) {
      setAlert("Mahasiswa baru berhasil diubah");
      document.getElementsByClassName("editCancel")[0].click();
      const prevStudents = value.students.filter(student => {
        return student.id != editStudent.id
      })

      prevStudents.push(result);
      value.setTheStudents(prevStudents);
    }
  }

  return (
    <>
      <CheckedContext.Provider value={{
        checkedStudent,
        setCheckedStudent
      }}>
      {/* <!-- Add Modal HTML --> */}
      <div id="addStudentModal" className="modal fade">
        <div className="modal-dialog">
          <div className="modal-content">
            <form onSubmit={handleAddSubmit}>
              <div className="modal-header">
                <h4 className="modal-title">Form Pendaftaran Mahasiswa Baru</h4>
                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">
                  &times;
                </button>
              </div>

              <div className="modal-body">

                <div className="alert alert-secondary">
                  <strong>Data Diri</strong>
                </div>
                <div className="row">
                  <div className="col-sm-12">
                    <div className="form-group">
                        <label>Foto Formal</label> <br/>
                        <CldUploadButton 
                        uploadPreset="b4xaubpo" 
                        className={`h-48 col-sm-1 border-dotted bg-light rounded-md p-4 relative ${saveStudent.image && "pointer-events-none"}`}
                        onUpload={handleImageUpload}>
                          <div>
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-file-image" viewBox="0 0 16 16">
                              <path d="M8.002 5.5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0"/>
                              <path d="M12 0H4a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2M3 2a1 1 0 0 1 1-1h8a1 1 0 0 1 1 1v8l-2.083-2.083a.5.5 0 0 0-.76.063L8 11 5.835 9.7a.5.5 0 0 0-.611.076L3 12z"/>
                            </svg>
                          </div>

                          {saveStudent.image &&
                            <Image src={saveStudent.image} alt="Student Image" fill className="absolute object-cover inset-0"/>
                          }
                        </CldUploadButton>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-8">
                    <div className="form-group">
                      <label>Nama Lengkap</label>
                      <input value={saveStudent.nama} onChange={handleSaveChange} type="text" className="form-control" name="nama" placeholder="Masukan Nama Lengkap" required />
                    </div>
                  </div>
                  <div className="col-sm-4">
                    <div className="form-group">
                      <label>Nomor Identitas (NIK)</label>
                      <input value={saveStudent.nik} onChange={handleSaveChange} type="text" className="form-control" name="nik" placeholder="Masukan Nomor NIK" required pattern="\d{16}"/>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-5">
                    <div className="form-group">
                      <label>Tempat Lahir</label>
                      <input value={saveStudent.tempat_lahir} onChange={handleSaveChange} type="text" className="form-control" name="tempat_lahir" placeholder="Masukan Tempat Lahir" required />
                    </div>
                  </div>
                  <div className="col-sm-3">
                    <div className="form-group">
                      <label>Tanggal Lahir</label>
                      <input value={saveStudent.tanggal_lahir} onChange={handleSaveChange} type="date" className="form-control" name="tanggal_lahir" required />
                    </div>
                  </div>
                  <div className="col-sm-4">
                    <div className="form-group">
                      <label>Jenis Kelamin</label>
                      <select value={saveStudent.jenis_kelamin} onChange={handleSaveChange} className="form-control" name="jenis_kelamin" required>
                        <option value="" disabled selected>
                          Pilih Jenis Kelamin
                        </option>
                        <option value="laki-laki">Laki-laki</option>
                        <option value="perempuan">Perempuan</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-5">
                    <div className="form-group">
                      <label>Kewarganegaraan</label>
                      <select value={saveStudent.kewarganegaraan} onChange={handleSaveChange} className="form-control" name="kewarganegaraan" required>
                        <option value="" disabled selected>
                          Pilih Kewarganegaraan
                        </option>
                        <option value="wni">Warga Negara Indonesia (WNI)</option>
                        <option value="wna">Warga Negara Asing (WNA)</option>
                      </select>
                    </div>
                  </div>
                  <div className="col-sm-3">
                    <div className="form-group">
                      <label>Agama</label>
                      <select value={saveStudent.agama} onChange={handleSaveChange} className="form-control" name="agama" required>
                        <option value="" disabled selected>
                          Pilih Agama
                        </option>
                        <option value="islam">Islam</option>
                        <option value="kristen">Kristen</option>
                        <option value="hindu">Hindu</option>
                        <option value="budha">Budha</option>
                        <option value="lainnya">Lainnya</option>
                      </select>
                    </div>
                  </div>
                  <div className="col-sm-4">
                    <div className="form-group">
                      <label>Nama Ibu Kandung</label>
                      <input value={saveStudent.nama_ibu} onChange={handleSaveChange} type="text" className="form-control" name="nama_ibu" placeholder="Masukan Nama Ibu Kandung" required />
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-5">
                    <div className="form-group">
                      <label>Email</label>
                      <input value={saveStudent.email} onChange={handleSaveChange} type="email" className="form-control" name="email" placeholder="Masukan Email" required />
                    </div>
                  </div>
                  <div className="col-sm-3">
                    <div className="form-group">
                      <label>No Telp</label>
                      <input value={saveStudent.no_telp} onChange={handleSaveChange} type="text" className="form-control" name="no_telp" placeholder="Masukan No Telp" required />
                    </div>
                  </div>
                </div>

                <div className="alert alert-secondary">
                  <strong>Data Alamat Asal</strong>
                </div>
                <div className="row">
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>Alamat</label>
                      <textarea value={saveStudent.alamat} onChange={handleSaveChange} className="form-control" name="alamat" rows="2" required></textarea>
                    </div>
                  </div>
                  <div className="col-sm-2">
                    <div className="form-group">
                      <label>Kode Pos</label>
                      <input value={saveStudent.kode_pos} onChange={handleSaveChange} type="text" className="form-control" name="kode_pos" placeholder="Kode Pos" required />
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-4">
                    <div className="form-group">
                      <label>Provinsi</label>
                      <input value={saveStudent.provinsi} onChange={handleSaveChange} type="text" className="form-control" name="provinsi" placeholder="Masukan Provinsi" required />
                    </div>
                  </div>
                  <div className="col-sm-4">
                    <div className="form-group">
                      <label>Kabupaten</label>
                      <input value={saveStudent.kabupaten} onChange={handleSaveChange} type="text" className="form-control" name="kabupaten" placeholder="Masukan kabupaten" required />
                    </div>
                  </div>
                  <div className="col-sm-4">
                    <div className="form-group">
                      <label>Kecamatan</label>
                      <input value={saveStudent.kecamatan} onChange={handleSaveChange} type="text" className="form-control" name="kecamatan" placeholder="Masukan Kecamatan" required />
                    </div>
                  </div>
                </div>

                <div className="alert alert-secondary">
                  <strong>Data Pendidikan</strong>
                </div>
                <div className="row">
                  <div className="col-sm-4">
                    <div className="form-group">
                      <label>Pendidikan Terakhir</label>
                      <select value={saveStudent.pendidikan} onChange={handleSaveChange} className="form-control" name="pendidikan" required>
                        <option value="" disabled selected>
                          Pilih
                        </option>
                        <option value="SMA-IPA">SMA - IPA</option>
                        <option value="SMA-IPS">SMA - IPS</option>
                        <option value="SMK-Mesin">SMK - Mesin</option>
                        <option value="SMK-Teknik Elektronika">SMK - Teknik Elektronika</option>
                        <option value="lainnya">Lainnya</option>
                      </select>
                    </div>
                  </div>
                  <div className="col-sm-4">
                    <div className="form-group">
                      <label>Nama Sekolah</label>
                      <input value={saveStudent.sekolah} onChange={handleSaveChange} type="text" className="form-control" name="sekolah" placeholder="Masukan Nama Sekolah" required />
                    </div>
                  </div>
                  <div className="col-sm-4">
                    <div className="form-group">
                      <label>Rata-rata Nilai Rapor Kelas 12</label>
                      <input value={saveStudent.nilai} onChange={handleSaveChange} type="text" className="form-control" name="nilai" placeholder="Masukan Rata-rata Nilai Rapor" required />
                    </div>
                  </div>
                </div>

                <div className="alert alert-secondary">
                  <strong>Pilihan Program Studi</strong>
                </div>
                <div className="row">
                  <div className="col-sm-4">
                    <div className="form-group">
                      <label>Pilih Program Studi 1</label>
                      <select value={saveStudent.program_studi_1} onChange={handleSaveChange} className="form-control" name="program_studi_1" required>
                        <option value="" disabled selected>
                          Pilih
                        </option>
                        <option value="S1-Informatika">S1 - Informatika</option>
                        <option value="S1-Teknik Industri">S1 - Teknik Industri</option>
                        <option value="S1-Teknik Sipil">S1 - Teknik Sipil</option>
                        <option value="S1-Teknik Elektro">S1 - Teknik Elektro</option>
                        <option value="S1-Teknik Geologi">S1 - Teknik Geologi</option>
                      </select>
                    </div>
                  </div>
                  <div className="col-sm-4">
                    <div className="form-group">
                      <label>Pilih Program Studi 2</label>
                      <select value={saveStudent.program_studi_2} onChange={handleSaveChange} className="form-control" name="program_studi_2" required>
                        <option value="" disabled selected>
                          Pilih
                        </option>
                        <option value="S1-Informatika">S1 - Informatika</option>
                        <option value="S1-Teknik Industri">S1 - Teknik Industri</option>
                        <option value="S1-Teknik Sipil">S1 - Teknik Sipil</option>
                        <option value="S1-Teknik Elektro">S1 - Teknik Elektro</option>
                        <option value="S1-Teknik Geologi">S1 - Teknik Geologi</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              <div className="modal-footer">
                <input type="button" className="btn btn-default addCancel" name="submit" data-dismiss="modal" value="Cancel" />
                <input type="submit" className="btn btn-success" value="Add" />
              </div>
            </form>
          </div>
        </div>
      </div>
      {/* <!-- Edit Modal HTML --> */}
      <div id="editStudentModal" className="modal fade">
        <div className="modal-dialog">
          <div className="modal-content">
            <form onSubmit={handleEditSubmit}>
              <div className="modal-header">
                <h4 className="modal-title">Edit Student</h4>
                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">
                  &times;
                </button>
              </div>
              <div className="modal-body">
                <input type="hidden" name="updateId" className="updateId" />

                <div className="alert alert-secondary">
                  <strong>Data Diri</strong>
                </div>
                <div className="row">
                  <div className="col-sm-12">
                      <div className="form-group">
                        <label>Foto Formal</label> <br/>
                        <CldUploadButton 
                        uploadPreset="b4xaubpo" 
                        className={`h-48 col-sm-1 border-dotted bg-light rounded-md p-4 relative ${editStudent.image && "pointer-events-none"}`}
                        onUpload={handleImageUpload}>
                          <div>
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-file-image" viewBox="0 0 16 16">
                              <path d="M8.002 5.5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0"/>
                              <path d="M12 0H4a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2M3 2a1 1 0 0 1 1-1h8a1 1 0 0 1 1 1v8l-2.083-2.083a.5.5 0 0 0-.76.063L8 11 5.835 9.7a.5.5 0 0 0-.611.076L3 12z"/>
                            </svg>
                          </div>

                          {editStudent.image &&
                            <Image src={editStudent.image} alt="Student Image" fill className="absolute object-cover inset-0"/>
                          }
                        </CldUploadButton>
                      </div>              
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-8">
                    <div className="form-group">
                      <label>Nama Lengkap</label>
                      <input type="text" value={editStudent.nama} onChange={handleEditChange} className="form-control updateNama" name="nama" placeholder="Masukan Nama Lengkap" required />
                    </div>
                  </div>
                  <div className="col-sm-4">
                    <div className="form-group">
                      <label>Nomor Identitas (NIK)</label>
                      <input type="text" value={editStudent.nik} onChange={handleEditChange} className="form-control updateNIK" name="nik" placeholder="Masukan Nomor NIK" required />
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-5">
                    <div className="form-group">
                      <label>Tempat Lahir</label>
                      <input type="text" value={editStudent.tempat_lahir} onChange={handleEditChange} className="form-control updateTempatLahir" name="tempat_lahir" placeholder="Masukan Tempat Lahir" required />
                    </div>
                  </div>
                  <div className="col-sm-3">
                    <div className="form-group">
                      <label>Tanggal Lahir</label>
                      <input type="date" value={editStudent.tanggal_lahir} onChange={handleEditChange} className="form-control updateTanggalLahir" name="tanggal_lahir" />
                    </div>
                  </div>
                  <div className="col-sm-4">
                    <div className="form-group">
                      <label>Jenis Kelamin</label>
                      <select value={editStudent.jenis_kelamin} onChange={handleEditChange} className="form-control updateJenisKelamin" name="jenis_kelamin" required>
                        <option value="" disabled selected>
                          Pilih Jenis Kelamin
                        </option>
                        <option value="laki-laki">Laki-laki</option>
                        <option value="perempuan">Perempuan</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-5">
                    <div className="form-group">
                      <label>Kewarganegaraan</label>
                      <select value={editStudent.kewarganegaraan} onChange={handleEditChange} className="form-control updateKewarganegaraan" name="kewarganegaraan" required>
                        <option value="" disabled selected>
                          Pilih Kewarganegaraan
                        </option>
                        <option value="wni">Warga Negara Indonesia (WNI)</option>
                        <option value="wna">Warga Negara Asing (WNA)</option>
                      </select>
                    </div>
                  </div>
                  <div className="col-sm-3">
                    <div className="form-group">
                      <label>Agama</label>
                      <select value={editStudent.agama} onChange={handleEditChange} className="form-control updateAgama" name="agama" required>
                        <option value="" disabled selected>
                          Pilih Agama
                        </option>
                        <option value="islam">Islam</option>
                        <option value="kristen">Kristen</option>
                        <option value="hindu">Hindu</option>
                        <option value="budha">Budha</option>
                        <option value="lainnya">Lainnya</option>
                      </select>
                    </div>
                  </div>
                  <div className="col-sm-4">
                    <div className="form-group">
                      <label>Nama Ibu Kandung</label>
                      <input value={editStudent.nama_ibu} onChange={handleEditChange} type="text" className="form-control updateNamaIbu" name="nama_ibu" placeholder="Masukan Nama Ibu Kandung" required />
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-5">
                    <div className="form-group">
                      <label>Email</label>
                      <input value={editStudent.email} onChange={handleEditChange} type="email" className="form-control updateEmail" name="email" placeholder="Masukan Email" required />
                    </div>
                  </div>
                  <div className="col-sm-3">
                    <div className="form-group">
                      <label>No Telp</label>
                      <input value={editStudent.no_telp} onChange={handleEditChange} type="tel" className="form-control updateNoTelp" name="no_telp" placeholder="Masukan No Telp" required />
                    </div>
                  </div>
                </div>

                <div className="alert alert-secondary">
                  <strong>Data Alamat Asal</strong>
                </div>
                <div className="row">
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>Alamat</label>
                      <textarea value={editStudent.alamat} onChange={handleEditChange} className="form-control updateAlamat" name="alamat" rows="2" required></textarea>
                    </div>
                  </div>
                  <div className="col-sm-2">
                    <div className="form-group">
                      <label>Kode Pos</label>
                      <input value={editStudent.kode_pos} onChange={handleEditChange} type="text" className="form-control updateKodePos" name="kode_pos" placeholder="Kode Pos" required />
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-4">
                    <div className="form-group">
                      <label>Provinsi</label>
                      <input value={editStudent.provinsi} onChange={handleEditChange} type="text" className="form-control updateProvinsi" name="provinsi" placeholder="Masukan Provinsi" required />
                    </div>
                  </div>
                  <div className="col-sm-4">
                    <div className="form-group">
                      <label>Kabupaten</label>
                      <input value={editStudent.kabupaten} onChange={handleEditChange} type="text" className="form-control updateKabupaten" name="kabupaten" placeholder="Masukan kabupaten" required />
                    </div>
                  </div>
                  <div className="col-sm-4">
                    <div className="form-group">
                      <label>Kecamatan</label>
                      <input value={editStudent.kecamatan} onChange={handleEditChange} type="text" className="form-control updateKecamatan" name="kecamatan" placeholder="Masukan Kecamatan" required />
                    </div>
                  </div>
                </div>

                <div className="alert alert-secondary">
                  <strong>Data Pendidikan</strong>
                </div>
                <div className="row">
                  <div className="col-sm-4">
                    <div className="form-group">
                      <label>Pendidikan Terakhir</label>
                      <select value={editStudent.pendidikan} onChange={handleEditChange} className="form-control updatePendidikanTerakhir" name="pendidikan_terakhir" required>
                        <option value="" disabled selected>
                            Pilih
                        </option>
                        <option value="SMA-IPA">SMA - IPA</option>
                        <option value="SMA-IPS">SMA - IPS</option>
                        <option value="SMK-Mesin">SMK - Mesin</option>
                        <option value="SMK-Teknik Elektronika">SMK - Teknik Elektronika</option>
                        <option value="lainnya">Lainnya</option>
                      </select>
                    </div>
                  </div>
                  <div className="col-sm-4">
                    <div className="form-group">
                      <label>Nama Sekolah</label>
                      <input value={editStudent.sekolah} onChange={handleEditChange} type="text" className="form-control updateSekolah" name="sekolah" placeholder="Masukan Nama Sekolah" required />
                    </div>
                  </div>
                  <div className="col-sm-4">
                    <div className="form-group">
                      <label>Rata-rata Nilai Rapor Kelas 12</label>
                      <input value={editStudent.nilai} onChange={handleEditChange} type="text" className="form-control updateNilai" name="nilai" placeholder="Masukan Rata-rata Nilai Rapor" required />
                    </div>
                  </div>
                </div>

                <div className="alert alert-secondary">
                  <strong>Pilihan Program Studi</strong>
                </div>
                <div className="row">
                  <div className="col-sm-4">
                    <div className="form-group">
                      <label>Pilih Program Studi 1</label>
                      <select value={editStudent.program_studi_1} onChange={handleEditChange} className="form-control updateProgramStudi1" name="program_studi_1" required>
                        <option value="" disabled selected>
                          Pilih
                        </option>
                        <option value="S1-Informatika">S1 - Informatika</option>
                        <option value="S1-Teknik Industri">S1 - Teknik Industri</option>
                        <option value="S1-Teknik Sipil">S1 - Teknik Sipil</option>
                        <option value="S1-Teknik Elektro">S1 - Teknik Elektro</option>
                        <option value="S1-Teknik Geologi">S1 - Teknik Geologi</option>
                      </select>
                    </div>
                  </div>
                  <div className="col-sm-4">
                    <div className="form-group">
                      <label>Pilih Program Studi 2</label>
                      <select value={editStudent.program_studi_2} onChange={handleEditChange} className="form-control updateProgramStudi2" name="program_studi_2" required>
                        <option value="" disabled selected>
                          Pilih
                        </option>
                        <option value="S1-Informatika">S1 - Informatika</option>
                        <option value="S1-Teknik Industri">S1 - Teknik Industri</option>
                        <option value="S1-Teknik Sipil">S1 - Teknik Sipil</option>
                        <option value="S1-Teknik Elektro">S1 - Teknik Elektro</option>
                        <option value="S1-Teknik Geologi">S1 - Teknik Geologi</option>
                      </select>
                    </div>
                  </div>
                </div>

              </div>
              <div className="modal-footer">
                <input type="button" name="submit" className="btn btn-default editCancel" data-dismiss="modal" value="Cancel" />
                <input type="submit" className="btn btn-info" value="Save" />
              </div>
            </form>
          </div>
        </div>
      </div>

      <div className="container-xl">
        <div className="table-responsive d-flex flex-column">
          <Alert text = {alert} setAlert = {setAlert} style = {alert.length > 0 ? 'block' : 'none'} />
          <div className="table-wrapper">
            <Navbar searchQuery = {searchQuery} setSearchQuery={setSearchQuery} setAlert = {setAlert} setCurrentPage={setCurrentPage}/>
            <StudentTable setEditStudent = {setEditStudent} students = {paginatedStudent} handleDelete = {handleDelete} />
            <Pagination 
              studentsCount = {searchQuery.length > 0 ? searchedResult.length : value.students.length} 
              currentPage = {currentPage} 
              pageSize = {pageSize} 
              onPageChange = {onPageChange} 
            />
          </div>
        </div>
      </div>
      </CheckedContext.Provider>
    </>
  );
}

export default Layout;
