function Alert({text, style, setAlert}) {

	const handleAlert = (e) => {
		e.preventDefault();
		setAlert("");
	}

    return (
        <div className="alert w-100 align-self-center alert-success alert-dismissible fade show" style={{display : style}} role="alert">
			{text}
			<button type="button" className="close" onClick={(e) => handleAlert(e)}>
			  <span aria-hidden="true">&times;</span>
			</button>
		</div>
    )
}

export default Alert;